# sGTM - UA - Plugins Client

## Purpose
If you serve UA from sGTM and need to use plugins, this will not work for some inexplicit reason. This template solves this issue.

## Installation
Import the template file under Client Templates. Then, under Clients, create a new client and select 'UA - Plugins' as client type. Now, all request to https://[your-tag-manager-hostname]/plugins/ua/[plugin-name].js will return the google version. E.g https://collect.mydomain.com/plugins/ua/ec.js
