﻿___TERMS_OF_SERVICE___

By creating or modifying this file you agree to Google Tag Manager's Community
Template Gallery Developer Terms of Service available at
https://developers.google.com/tag-manager/gallery-tos (or such other URL as
Google may provide), as modified from time to time.


___INFO___

{
  "type": "CLIENT",
  "id": "cvt_temp_public_id",
  "version": 1,
  "securityGroups": [],
  "displayName": "UA - Plugins",
  "brand": {
    "id": "brand_dummy",
    "displayName": "",
    "thumbnail": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAW6klEQVR42u2deZBURbaHL5sxgvOeS7zReaLzXoyho8OMyOj7z/GPNy7QpwBb61Q3+9KnuptFhkWkkVXgOSOKsrmMo7LI4sIO7sruIIsgm4CAiiiCdONGb9StepFwWwtomqrqW1U3b/5OxBdGELJ03vxO5s2bedKyEFoHhcL/RiwtiKUNsfQklhJieYJYXiSWpcSynFjWEss6YtlILJuJZQuxbCWWbQ5bnV/b7Pw/65zfs9z5M150/swS5+9Qf1dLYrkUTwCBSHMEQuFfEMuNxJJPLGOIZS6xbCCWMmKJZJky59+i/k3jiKUzsfwpwOFmeHIIRNIjujQllluJZSCxTHdG5yoPiJ4s1cSyk1hmEcsgYrktwOFf4gkjEHGRE5RfEcs9xDLJmX5Xaih7olQ5rxnqZ2Vi+TV6AMKw6XxBM2K5i1jGO6P7CR8Lfz5OOG3wOLHkBFjw2oDw4bSew1cSSyGxLCaWHwwW/nwcJ5ZlxNKLQnI1eg5C36k9y9XOe/xqw0f5+swO3nfWD/4bPQqhwUgvlzkj/QpI73oyWH1yZsAFl6GnIbwjfaiwkfNOP8eZwkLY9FJOLC8TS+t2HcON0AMR2RrtLyeWIcSyD1JmjU+J5UF8TUBkUvybiWUGsVRAQM9Q4exW/B/0UITr0Ta/oAGxBIhlJWTzPGqtoF3r3IKG6LmI+onPRY2JpQuxfASxtGO7enaBkDRBT0YkN83PK1TidyOWXRBJe/YQS4+cUBiJAFF3tMvr3pBY8iC+L9lNLB0CQbwaIGpf3LuDWDZBFN+jziLchR6PqBG/hbP9FHKYxevE8gcYYKz44UucIheVkMFY1OnEyQEUNjEn7gz2VJ/0uhPLIQgAHA6rSketg90bwBB/T/evd8pcodOD2lD7PG6AKf77rNeEWEY4+8jR0cH5zhqMIpYLYI4/Rv0/YXUfpPi1oBUM0vabvqjNPMOwZx/UA7VAPJzyihrDKL1G/WucstboxMANVGGSa2CWHvJ3JJZj6LTAZVSf6gTDvCu+Kqf9AjoqSDPTiAtQvNRj8l+LE3sgg6i+9juY5w35czHlB1l6JbgHBmZtlf/kyb3RKL4Jslys9CGcMMxwBFguIpYF6IDAIyzI4fBFMDMzU/7m2NgDPMimAIevgqHplV9dSf0FOhvwKKpv3gRT0yP/X7DYBzRZHLwdxrorfx629ALNypTnw1x35C9y7ppHxwI6ofpsMQyun/wPoCMBzXkAJqcm/zB0HuATRsLo5OQfjU4DfMZDMDsx+YejswCfMgKG450fYE0AcY7VfnQQYAL4OnCG/Pn41AeM+kQYxD6BGvlvxyYfYOhmodtNl78ltvcCw7cNtzRV/qtwsMcseg8cGZn9ymJ73YbN0e07d0fVf+e8stjuM2iUbfgBouZGyR/gsDrPvxFSmEHn8MDI2nUbo7FYNHYuVDLoUjjI3KPEQTGjnkCb3J4NUczDHIr7j4gcLS2N1SV/DaVlx2K9Bow0tqiIEZWF1I4oiGGI/ANGRI4d+zYh+Ws4fOSbWF73ftgt6FP5c1HDzxz5y5KUv4Ylr78bNbjGYK4v5c9huQ4r/hj5E6GyqjLGXfua+2UgKNf5TP6CZqjbj5E/GcY+MtXkLwMfUVCa+WnqPw1yYORPhhdmvWob3p4v+EX+TpAD8ifLrJcX2WhXze8idG7pxXs/pv1JM+UfM5AATrmj563EbTnc2LlWGZJA/qTpPXAkEsAp3g/kS2MU9gDGyL93/2dRtO9pDNNN/lY44Qf5UyEatWPDxkzA6H86lcopPeQPFV5ALJvx0CB/Krw0fylG/9rZTHmFTXQY/UfiYUH+VFi07G3Ir3NNQWK5gVjK8aAgP+RPC8qt6715xDc/3IBYVuAhQX7In1ZWKNe8OPr3wMNxH3U8ds6rS6IbN2+N7v5kX3TL1p3Rhcvejj4w8hHbD/IvhPyp0N1je/3DlxDL13gw7tGhZ//IyrUfRNWq+Lnk2bnrk2hR/+EY+c3jkHLOS6P/JDwU9+jRe0jkq68PJyTR8fLyWMnoR23IbxwTPSJ/uAWxVOGBuENup16Rzw58EU1GporKythQl5MA5Ndib0ALL4z+r+FhuMfMuQvslM7Qu5gEIL82LMu2/HfiIbhHu7zCyLFvUxdPzQTq+zoA+bXjjmwW9/wQD8A9BgwdZ9dXssp6JAHIr2dF4da5WSgm6lznhQfgIo9Nfs52Q7ZUkgDk15q8DMtf0IRYdqPh3WXS09Ntt6SrqKxIOAlAfu3Z1Y4zeGRYbURAo7vPqIcn2m7Kl8hMAPL7hm4ZkV9lGmLZgwZ3n/wef41EIidimUoCkN9X7G7LhY0zMfp3QWOnj3+t/zDqtoy1vQ5Afl/SOb3yB4vUyv82NHR69/5XV1e7LmX8TADy+5atd+aGG6Zz9G+HRk4/E5+aZqdDTpUEJj8zw4b8vqZtOhPAajRwZnj6+dl2NBpNi6iQ39esTJf8t6BxkQQgvxbcko4EMBMNm3lUjTzID5JkpsuLf3IFqvxmnnC/ByNHS8sgP0iWigDLFW6O/kPRqJAf8mtFiTs3/OQXNyKW/WhQyA/5tWJ/TqiokRujf2s0JuSH/FpylxsJ4CU0JOSH/Foyt77lvi4jluNoSMgP+bVEuXtZfUb/YjQi5If8WlNUnwSwCg0I+fGctGZFqvL/F7GcQAOaK79tR1yvNgwyjnL46lQSwCA0nrnyp6PaMMgaA1JJAGvRcGbLjyTgG9Yku/rfHNN/yO9WtWHd6T1olD3lHzPtWS8vsqfNmmePHT/V5q59tXoNyAnJlVj9h/z1rCxkVhIYNmaCvXf/Z7UexqqsqowteePdaF73fv77GkAsSyEs5Dd5JjB33tI6L2at4fA3R2O9BozQ4Wdakqj8zYjlR0gL+U1dE1i49K2kjmCXlh2LdSkc5PWf6wcKFTRNJAG0gbSQPxsXknpC/mVvpVR/QRVz1eDna51IApgAcSG/iTOBZEf+M1GLhR7/GR9LJAFshbyQ37SZQKojfzyzX1ns9Xb4KJHKP/j8B/mNmgnUd+SvYd2GzV5/DVBuX17X6B+EwJDfpCTglvyKbTt26bAOcG9dCWASJIb8piQBN+XXZAagmFhXAtgEkSG/CWsCbrzzn4naKahBX91Ye+0/Dqvv/1WQGfL7fSbg9sivUPc3qGvdNOivlYFQQbOzEkAOy22QGfL7fSaQjpFfsXbdRp3qJvy5tun/QAgN+f08E0jHyK/4prQ01jk8UKe+O7C2BPAipE6cnn1KIkdLSyG6JjOBdMl/7NtvY8V6nAWIZ2ZtCWA7xE6MrkX3Rw4dPgLBNZkJpFN+Td77z2Tb6fLnh5tiATAxgl36RPZ9+nkUYuuRBCB/rVQF8sIXxo/+N0Hu8xMIhSPrN22B/JokgfTKP0Lv/hyUlvEJoCMEPz/qOy9E1iMJYOQ/Lx3iE8BYCF43qtPatg2JNUgCkD8hxsQngLmQ/NyEut0XOfLNUcirQRKA/AkzJz4BbIDo5+bNd1fhvV+DT4R450+K9fEJoAyi184DIx+xE6kNB7I7E8DInzSlJ+UPhOQSiH7uVX988vN+EoD8KfZvDl+sRv8/QPbaefzJF7Dq7/EkAPnrRQsUAT0H7fKLIl9jt5+nkwDkd6FIKLH0hPBnM/GpaRj9PZwEsODnCj1UAhgK4c/m8wMH8e7v0SSAkd81SlQCeALCn86DYyZg9PdoEsDI7yqP4xhwLaxc+wFGfw8mAYz8aTgWjHsAz9j1171fpKq6CgJ6LAlg5E8LS1QCeBfi/8yEKc9h+u8xbDsSg/xpYbVKAO9B/J9Rd7xBOv8D+U+yFQkgjvYdiiLlFRUQxAj5R6LPs+xFAjhj3z8EgfwG8aVKAMvREKeYPmc+EgDkN4ljSABxqKudIArkN4gfkQDiQNEPyG8YFUgANdV+u/aN4Nw/5DeMaiQAh/sGP4T3f8iPBGAqY8dPRQKA/HgFMJWpz76IBIBNPlgENJUZcxYgAfiEyc/MsNGnE/8MiI1ALJFXF76OBOCbasMVnrya3KsbgZAA0njUFOBqcmwF1oDFr72DBODDJFCCJHDew0A4DswSmb/kTSQAzASMPA6MgiAskbnzlmANADMBIwuCoCQYS+Sf019CAsBMwMiSYCgKikpASAJmcrIoaAkaQiJqighJkAQMYwguBnHo2ackAkGQBAyjB64Gi7sIVHUMCIIkYBCtrRxcDvoTe/Z9ik+B+DpgDkFpYakrgiH/KZa9tRwJwDCOl5fHivoPN3TWW/jvlgpiKUMCwJcAU9m565Oogf291KoJYtmABCCRbsWDsRBoKKoitGH9fX18ApiLBHCKLw5+hdcAA1m47G3TZgFz4hPAWMjvHAte9DoSgIFs2brDtAQwJj4BdIT8pxgwdBzWAQxk9yf7TEsAHeITwE2Q/2cOfnkIUhjGhg+3mpYAWv6UANpy4YXEUgX5nRuCZs/DLMAw5ry6xKRFwCrKD19oxQexbIf8p+goAyLV1VUQQxNKy8piL81fFo1GU/v96j6IYrOKiG6zzgwcCz6dt5evwWKgBhwtK4sV9ht28pk98/yclGZuK9d+YNr0f2ZtCWAgxP8ZtTvMtiOQzOMjf438NTzz/Gw7mZnAoa+PxDr07G9a/x5QWwL4M8Q/nRVr1mEWoMHIfyYTn5pmVyXwCvfZgS+iPXoPMbFv33p2AggVNCOWSoj/MwV9h0aqT1RDOA/KH+73YJ3PTt0K9K/1H0YjkUitF4fMnLvAzu3Uy8R+XRlgaWbVFsSyCeKfUSx0MYqF6iZ/PPk9/hoZ9fBEe9LT0+3HJv/TVvs82uYVmtynN1jnCmKZBOnPuDW4S5/IkaO4Ntyr7/wgaSbWlQDuRQOdzei/T8a+AMjvF+6pKwFcTiwn0Ehn8+a7q/AqAPl1R7n9K6uuULeFoKHO5t4ufSJffPkVhIT8OvORdb4glgloqNpRu8UqKiogJuTXlUcTSQAoEloHY8dPtW3bhqCQX0fuOm8CUN8IieVHNNa5eRa3CGVtkw9ImR+Ieza1Egl1ZxgarG5emr8Ui4IY+XVisZVoEEsRGuw8+wO69o2Ulh2DtJBfFwqTSADh5vgcWPfmoB0f78EMAPLr8/kvGL7SSiaIZQ0aDvJDfl+w2ko2cDwY8kN+39A/lQTwG7wGQH7I74vdf1dbqQSxrEQDQn7IrzUrrFQDXwMgP+Q3aPW/lgRwGbEch/yQFvJryfFAUC616hOmXhsG+SG/r67/qkcCaA35AeT36d7/80W7juFGxLIf8gPIrxX7AvnSyHIjiGUo5K+vBGZuGz5aCvmzRInlVlBQriCWCr/Lvz1N8u/Y9UlUnR2YO8+sA0RfHz4SK+hbAhkzTzmF5ArLzSCWGZA/dfl/Oko8ba4R9QT2f/p5tGvR/ZAxO8yw3A5iuRnT/vrJX8O48VPtch9XFlq/6aNosEtfiJg9brbSEcSyym91/rZnWP6fL64YETlw8Etfia9mNrNeXmQHQmFImD1WWukKYmnrJ/l3ZEn++NnHG++s9MW6wJFvjsZKRj9qQ8CsE0hbAmibX9BAXS2seyOpK6G279ydJvn3JCT/afcO/G2yrQTSUXx1tbYqmR7qdh/kyz5b2wR7NbDSGcTSWfeGemfF2vTJn+K7r5oNzFv8RrQ6gUstPbPQ99nn0cEj/o5R3zt0ttIdgfyixsSyW9dGun/432yvyR+PupBU3Urs5avJDx0+Ent86vN41/cWu3LyihtbmQhi6aZrQ61c80HUq/LHozbPvPXeak/NCD4/cDD6xFPT7Hb5RRDOe3S1MhWBkDRRGUfHhvru++89L388HQsGRKbNmmdn61aiqqqq2Kq166PDxkzAVN+7fNyWw42tTAax5OnWUGqhSif5z6R/yTj75QXLogcOfpnWLwfl5eWxdRs2RydMec7G4p4WhKxMBwWLGhLLJp0aSt0Vr6v8Z9KteHBECbr0jfeie/buj1ZUVqa8in/4yDexDzZuiU6fPd9Wi3qY4mvFpta5BQ2tbASx3KFTY6lFq/KKcu3lP9fP1qP3kEjJqPH2Y1Oes9XtRXPnLbHnL34juvi1d6ILl74VfWXha/aMOfPtqc/OtMc8MsW+b/BDNnbsac/tVjaDWJbp1GAbN2+N+k1+YCxLrWwHsbQglipdGu3/Hn3ShvzAB1QSF/ze8kIQy0SdXgO27dgVhfxAc56wvBKBoFxCLId0abwuhYMi6qx6ovKrbcOQH3iIQ8QFF1teCmLpoVMjqiSQyExAbRvO7dQbnQ54iW6W1+Luu3urg0IrdPsqMO7RJ211fj3+68C3330XUzsGBw17GJtfgNd4L6dDcQPLi0Es158sR6Rhw6pkoPYJMDa+AC+X+uKC6y0vB7EMx4MCIC0Mt7wegbzwBcTyIR4WAK7yYYCliaVDEEsrv1cRBiCDKJdaWToFsQzDgwPAFR60dIu2fLJwyFo8PADqxdqMFfpIwyzgGmI5hocIQEood35r6RzE0hEPEoCU6Gj5IYjlBTxMAJLiOcsvQRxuSixb8FABSIgtyhnLT5HDch3WAwBI6L3/WsuPQSx3E8sJPGQAakW50d7ycxDLKDxoAGpllOX3CATDqpjofDxsAE5jftaKe2a+orBcRCwb8dABOIlyoZllUlBImhPLATx8YDgHlAuWiUEsNxJLGToBMBTV92+0TA5i+V9di4gAUM8Tfn+xECfXBNQ1Y9XoFMAQqlWfh/mnzwQK0TGAIRTB+NqTwGB0DuBzBsN0FBIBqOmHwG5BYBCjYTZmAgAjPyLJNQEcHgJ45zf86wA+EQL9PvVhtd+1JBDCZiGg2SaffJjr/o5BbBsGOmzvxQ6/NJ4dwAEi4N2DPSwtYWp6k0BzHCUG3jzSG24OQzMQbYI9L0JREeAhFgSChp3nz3ao6inEMhKfCUGWa/iNbpPbsyGMzN4rQXssDoIsVe9tDwO9kASCci3uHQAZRPW162Cel9YFQqIuH3kOnROkmedzggV43/fwK0E+LiABaZryd4JhWrwSFPyWWNag0wKXeF/ddA2zNIpAqLgRsTzobMtEJwapbukdlpNX3BhG6ftKcBOxbEJnBkmymVhawSA/JIG8wibObAAHisD5UH1keCBPLoA5/psN/I5Y3kMnB+dgObFcD1N8HO3bd2tALN2I5St0eOBwiFh6BPLDDWCIKYuELBcTy+PEUgkBjEU9+0nEcgmMMPe14AZiWQIZjOM1Yvk9DEDUJILb8bXACNQzvgM9HnF2EggWqROGQWL5GKL4jt3Ektc2rwdO7iHqjrZc1JhYuhDLLojjC/G7B0LSBD0bkUoi6EQsH0Ek7dhOLJ0D+UXYxYeoX9x9d5H6dEjEsgJieZ5VxNIOU31EuhYLWxHLdJwx8NzuvZnEcgt6KCIziSAklxPLEGLZBwGzxqfEMjTA8mv0SERWIpAv6tThncQym1iOQ8q0o9r4JWJp3S5Y2Ag9EOGl14NLiSXs7ClHsVJ3i2+qd/ti1cboaQgNkkH4KmLp73Rc3G2YmvRriWUAsfwGPQqh72sCh//TmRksIpbvIfc5+dHZlt0rB5dsIPyZDApU8dK7iGW8s7/ghOGjvGqDCcTSJsDSFD0EYdarQlD+g1hyiWUisaz3+enEKmcv/sSTW66DcgV6AAIRP0MIhi8klludd99pxLJV06SgZN9BLC8Sy0BiuY244CI8YQQi2VlCXuEviAv+SCwhYnmIWOY4s4VSD4he6vxb1L9pDLF0VHUYAyFM5xGI9CcHDv/SqWnQWlW1IZYSp9DJTGdBbbUzi9jrVEFS9e1/cHYyVjtUOL92zPl/9jq/Z7XzZ8x0/swhzt+h/q4/UgiFNHSP/weZM4UYF1VcbQAAAABJRU5ErkJggg\u003d\u003d"
  },
  "description": "Automatically claim UA plugins starting with the path /plugins/ua/ and proxy the plugin at on analytics.google.com",
  "containerContexts": [
    "SERVER"
  ]
}


___TEMPLATE_PARAMETERS___

[]


___SANDBOXED_JS_FOR_SERVER___

// Call the claimRequest API if this client should capture the request.
const getRequestPath = require('getRequestPath');
const returnResponse = require('returnResponse');
const setResponseBody = require('setResponseBody');
const sendHttpGet = require('sendHttpGet');
const setResponseHeader = require('setResponseHeader');
const setResponseStatus = require('setResponseStatus');
const queryPermission = require('queryPermission');
const logToConsole = require('logToConsole');

const path = getRequestPath();

if (path.indexOf('/plugins/ua/') === 0) {
    runClient(path); 
}

function runClient(path) {
    require('claimRequest')();

    sendHttpGet('https://www.google-analytics.com' + path, (statusCode, headers, body) => {
        setResponseBody(body);
        setResponseHeader('cache-control', headers['cache-control']);
        setResponseHeader('content-type', headers['content-type']);
        setResponseStatus(statusCode);
        returnResponse();
    }, {
        timeout: 500
    });

}


___SERVER_PERMISSIONS___

[
  {
    "instance": {
      "key": {
        "publicId": "read_request",
        "versionId": "1"
      },
      "param": [
        {
          "key": "bodyAllowed",
          "value": {
            "type": 8,
            "boolean": true
          }
        },
        {
          "key": "headersAllowed",
          "value": {
            "type": 8,
            "boolean": true
          }
        },
        {
          "key": "pathAllowed",
          "value": {
            "type": 8,
            "boolean": true
          }
        },
        {
          "key": "requestAccess",
          "value": {
            "type": 1,
            "string": "specific"
          }
        },
        {
          "key": "headerAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        },
        {
          "key": "queryParameterAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "return_response",
        "versionId": "1"
      },
      "param": []
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "access_response",
        "versionId": "1"
      },
      "param": [
        {
          "key": "writeResponseAccess",
          "value": {
            "type": 1,
            "string": "any"
          }
        },
        {
          "key": "writeHeaderAccess",
          "value": {
            "type": 1,
            "string": "specific"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "logging",
        "versionId": "1"
      },
      "param": [
        {
          "key": "environments",
          "value": {
            "type": 1,
            "string": "debug"
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "send_http",
        "versionId": "1"
      },
      "param": [
        {
          "key": "allowedUrls",
          "value": {
            "type": 1,
            "string": "specific"
          }
        },
        {
          "key": "allowGoogleDomains",
          "value": {
            "type": 8,
            "boolean": true
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  }
]


___TESTS___

scenarios: []


___NOTES___

Created on 04/11/2021, 16:11:38


